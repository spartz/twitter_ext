
import mock

import tw_content_tw_metrics


class QueryMock(object):
    """SQLAlchemy mock func
    """

    class ResultMock(object):

        result = []

        def filter(self, *args, **kwargs):
            return self
        def order_by(self, *args, **kwargs):
            return self
        def all(self):
            return self.result
        def first(self):
            try:
                return self.result[0]
            except IndexError:
                return None
        def one(self):
            try:
                return self.result[0]
            except IndexError:
                return None

    def __init__(self, table, result_dict):
        self.rm = self.ResultMock()

        for result in result_dict:
            if table.__name__ == result["table"]:
                  self.rm.result = [result["value"]]

    def filter(self, *args, **kwargs):
        return self.rm

    def all(self):
        return self.rm.all()


@mock.patch('tw_content_tw_metrics.databases')
@mock.patch('tw_content_tw_metrics.requests')
@mock.patch('tw_content_tw_metrics.session')
@mock.patch('tw_content_tw_metrics.twitter')
def test_tw_content_tw_metrics_main(twitter_mock, session_mock, requests_mock, database_mock):
    """
    """
    tweet_mock = mock.MagicMock()

    api_mock = mock.MagicMock()
    api_mock.GetUserTimeline = mock.MagicMock(side_effect=[[tweet_mock], []])
    limit_dict = {"resources": {"statuses": {"/statuses/user_timeline": {"remaining": 100, "reset": 1},
                                             "/statuses/show/:id": {"remaining": 100, "reset": 1}
                 }             }            }
    api_mock.GetRateLimitStatus = lambda: limit_dict

    twitter_mock.Api = mock.MagicMock(return_value=api_mock)

    script = tw_content_tw_metrics.Script("OMG", None, None, None)

    script.main()

    assert "TWContent" in  str(session_mock.add.call_args_list[0])


@mock.patch('tw_content_tw_metrics.databases')
@mock.patch('tw_content_tw_metrics.requests')
@mock.patch('tw_content_tw_metrics.session')
@mock.patch('tw_content_tw_metrics.twitter')
@mock.patch('tw_content_tw_metrics.func')
def test_tw_content_tw_metrics_backpopulate(func_mock, twitter_mock, session_mock, requests_mock, database_mock):
    """
    """

    tweet_mock = mock.MagicMock()
    tweet_mock.created_at_in_seconds = 1

    api_mock = mock.MagicMock()
    api_mock.GetUserTimeline = mock.MagicMock(side_effect=[[tweet_mock], []])
    limit_dict = {"resources": {"statuses": {"/statuses/user_timeline": {"remaining": 100, "reset": 1},
                                             "/statuses/show/:id": {"remaining": 100, "reset": 1}
                 }             }            }
    api_mock.GetRateLimitStatus = lambda: limit_dict

    twitter_mock.Api = mock.MagicMock(return_value=api_mock)

    script = tw_content_tw_metrics.Script("OMG", 1, "2001-01-01", "2001-01-01")

    script.find_closest_tweets = mock.MagicMock(return_value=[])
    script.find_tw_content_by_tweet = mock.MagicMock(return_value=[])

    script._stop_time = 0
    script.main()

    assert "TWContent" in  str(session_mock.add.call_args_list[0])

