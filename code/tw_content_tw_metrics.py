#!/usr/bin/env python
import argparse
import datetime
import logging
import os
import time
import twitter
import re
import requests
from sqlalchemy.sql.expression import func

from analytics_helper.databases import (
        TWContent,
        TWMetric,
        TWContentTWMetric,
        SocialMediaAccount,
        )

from analytics_helper import (databases, settings)

databases.init_db()

TWITTER = settings.api['TWITTER']


session = databases.Sessions['live_analytics']

logging.basicConfig()
LOGGER =  logging.getLogger(__name__)
LOGGER.setLevel(20)

class Script(object):

    def __init__(self, site_id, back_populate, start_date, stop_date):
        self.description = """
            Gathers all latest twitter content
            greater than the maximum tw_id inside
            of TWContent.  Populates relevant
            twitter metrics for all relevant
            twitter content (TWContent)
        """
        self.site_id = site_id
        self.back_populate = back_populate
        self.start_date = start_date
        self.stop_date = stop_date

        if self.site_id == 'OMG':
            self._sma = session.query(SocialMediaAccount).filter(
                    SocialMediaAccount.network_name=='Twitter').filter(
                            SocialMediaAccount.site_id==self.site_id).filter(
                                    SocialMediaAccount.account_name=='omgfacts').first()
        else:
            self._sma = session.query(SocialMediaAccount).filter(
                SocialMediaAccount.network_name=='Twitter').filter(
                        SocialMediaAccount.site_id==self.site_id).first()
        if not self._sma:
            LOGGER.error("No twitter account for site_id %s" % (self.site_id,))

        self._api = twitter.Api(
                consumer_key=TWITTER['consumer_key'],
                consumer_secret=TWITTER['consumer_secret'],
                access_token_key=TWITTER['access_token'],
                access_token_secret=TWITTER['access_token_secret']
                )

        self._tw_user = self._api.GetUser(
                user_id=self._sma.account_id
                )

        self._tw_metrics = session.query(TWMetric).all()

        self._list_re = re.compile(r'\w\/[0-9]+')
        self._list_id_re = re.compile(r'\/[0-9]+')

        self._timeline_requests = 0
        self._status_requests = 0
        self.get_limits()

        if back_populate:
            self._start_time = int(time.mktime(datetime.datetime.strptime(self.start_date, '%Y-%m-%d').timetuple()))
            self._stop_time = int(time.mktime(datetime.datetime.strptime(self.stop_date, '%Y-%m-%d').timetuple()))

    def main(self):
        """
        two phase process for gathering
        data as well as updating existing
        data
        """
        self.fetch_tweets()
        self.fetch_tweet_metrics()

    def GetUserTimeline(self, *arg, **kwargs):
        self.check_timeline_requests()
        self._timeline_requests += 1
        return self._api.GetUserTimeline(**kwargs)

    def GetStatus(self, *args, **kwargs):
        self._status_requests += 1
        return  self._api.GetStatus(**kwargs)

    def fetch_tweets(self):
        """
        Fetch any new tweets for the pertinent
        SocialMediaAccount
        """
        max_post_time = None
        try:
            max_post_time = session.query(func.max(TWContent.post_time)).filter(
                                          TWContent.sma_id==self._sma.id).first()[0]
        except (AttributeError, IndexError):
            LOGGER.error("No TWContent!")

        if max_post_time and not self.back_populate:
            self.fetch_tweets_current(max_post_time)
        else:
            self.fetch_tweets_back_populate()

    def fetch_tweets_current(self, max_post_time):
       twcontent_row = session.query(TWContent).filter(
               TWContent.sma_id==self._sma.id).filter(
                       TWContent.post_time==max_post_time
                       ).first()

       if not twcontent_row:
           LOGGER.error("No TWContent rows with post time %d" % (max_post_time,))
           return


       tweets = self.GetUserTimeline(
               user_id=self._tw_user.id,
               since_id=twcontent_row.tw_id
               )

       while len(tweets) > 0:
           for tweet in tweets:
               LOGGER.info("Processing tweet id {0} with text {1}".format(
                       tweet.id,
                       tweet.text
                       ))

               tw_content = self.process_tweet(tweet)
               self.process_tweet_metrics(tweet, tw_content)

           tweets = self.GetUserTimeline(
                   user_id=self._tw_user.id,
                   since_id=tweets[0].id
                   )

    def find_closest_tweets(self):
        return session.query(TWContent).filter(
                                TWContent.sma_id==self._sma.id).filter(
                                TWContent.post_time>=self._start_time
                                ).order_by(TWContent.post_time).first()

    def find_tw_content_by_tweet(self, tweet):
        return session.query(TWContent).filter(
                                           TWContent.tw_id==tweet.id
                                           ).first()

    def fetch_tweets_back_populate(self):

        closest = self.find_closest_tweets()

        user_dict_params = {"user_id": self._sma.account_id}

        if not closest:
            tweets = self.GetUserTimeline(**user_dict_params)
        else:
            user_dict_params["max_id"] = closest.tw_id
            tweets = self._api.GetUserTimeline(user_id=self._sma.account_id)

        while len(tweets):
            for tweet in tweets:
                tw_content = self.find_tw_content_by_tweet(tweet)

                if tw_content:
                    LOGGER.info("Had tweet with id {0} text {1}".format(tw_content.tw_id, tw_content.text))
                    continue

                if tweet.created_at_in_seconds > self._stop_time:
                    LOGGER.info("Processing tweet id {0} with text {1}".format(tweet.id, tweet.text))
                    tw_content = self.process_tweet(tweet)
                    self.process_tweet_metrics(tweet, tw_content)
                else:
                    break

            tweets = self.GetUserTimeline(
                user_id=self._sma.account_id,
                max_id=tweets[-1].id
                )

    def process_tweet(self, tweet):
        """
        @param: tweet
        @type: twitter.Status

        processes a Status object from
        the twitter python API and inserts
        into the DB
        """
        content_id = self.parse_tweet_content_id(tweet)
        retweet = self.is_retweet(tweet, content_id)
        twcontent = TWContent(
                sma_id=self._sma.id,
                tw_id=tweet.id,
                content_id=content_id,
                lang=tweet.lang,
                source=tweet.source,
                text=tweet.text,
                short_url=tweet.urls[0].url if tweet.urls != [] else None,
                url=tweet.urls[0].expanded_url if tweet.urls != [] else None,
                card=True if tweet.urls != [] and 'cards' in tweet.urls[0].expanded_url else False,
                post_time=tweet.created_at_in_seconds,
                created=int(time.time()),
                retweet=retweet
                )
        session.add(twcontent)
        try:
            session.flush()
            session.commit()
        except:
            session.rollback()
        return twcontent

    def fetch_tweet_metrics(self):
        """
        Fetch tweet metrics for newly
        acquired tweets as well as
        already existing tweets in the DB
        """

        self.get_limits()
        twcontent_rows = session.query(TWContent).filter(
                                       TWContent.sma_id==self._sma).filter(
                                       TWContent.post_time>=(int(time.time())-(86400*7)))

        for twcontent_row in twcontent_rows:

            now = int(time.time())
            twcontentmetric = session.query(TWContentTWMetric).filter(
                            TWContentTWMetric.tw_content_id==twcontent_row.id).filter(
                            TWContentTWMetric.created>=(now-(60*25))).first()
            if twcontentmetric:
                LOGGER.info("Retrieved tw metrics for tw_id {0} {1} minutes ago. skipping".format(
                        twcontent_row.tw_id,
                        int(float((now-twcontentmetric.created))/60.0)
                        ))
                continue

            self.check_status_requests()
            try:
                tweet = self.GetStatus(
                        id=twcontent_row.tw_id
                        )
                self.process_tweet_metrics(tweet, twcontent_row)
            except Exception, e:
                LOGGER.error(unicode(e))
                continue

    def process_tweet_metrics(self, tweet, twcontent):
        """
        @param: tweet
        @type: twitter Status object
        @param: twcontent
        @type: C{labs_app.models.TWContent} instance

        saves the relevant records in TWContentTWMetric
        """

        for metric in ['retweet_count', 'favorite_count', 'hashtags']:
            tw_metric = filter(lambda x: x.metric_literal == metric, self._tw_metrics)

            if not tw_metric:
                continue

            tw_metric = tw_metric[0]
            met_val = getattr(tweet, metric)

            if isinstance(met_val, int):
                count = met_val
            elif isinstance(met_val, list):
                count = len(met_val)
            else:
                LOGGER.error("error: metric %s value is of type %s" % (metric, type(met_val),))
                continue

            tw_content_tw_metric = TWContentTWMetric(
                    tw_content=twcontent,
                    tw_metric=tw_metric,
                    count=count,
                    event_time=int(time.time()),
                    created=int(time.time())
                        )
            session.add(tw_content_tw_metric)

            try:
                session.flush()
                session.commit()
                LOGGER.info("Saving metric {0} for tw_content_id {1}".format(
                        tw_metric.metric_name,
                        twcontent.id))
            except:
                session.rollback()

    def parse_tweet_content_id(self, tweet):
        """
        @param: tweet
        @type: twitter.api.Status

        returns Spartz content_id
        """
        hermes_api = os.getenv('HERMES_API', 'http://hermes.dose.com')
        hermes_api_key = os.getenv('HERMES_API_KEY', '92BVyr3E6zk0hLd6dpHU')
        urls = tweet.urls
        if urls != []:
            for url in urls:
                expanded = url.expanded_url

                try:
                    resp = requests.get(expanded)
                except requests.exceptions.ConnectionError:
                    LOGGER.error("Could not expand URL for %s" % (unicode(expanded),))
                    continue

                all_parsed_ids = re.findall("\.com/\w+/([0-9]+)\/", resp.url)

                if len(all_parsed_ids) != 1:
                    LOGGER.debug("There should be exactly one list id in the slug")
                else:
                    parsed_id = int(all_parsed_ids.pop().strip('/'))
                    res = requests.get(hermes_api + '/article/%s' % parsed_id, headers={'API-Key' : hermes_api_key})
                    if res.status_code < 400:
                        return parsed_id
        return None

    def get_limits(self):
        """
        Get the current number of requests
        left to use
        """

        limits = self._api.GetRateLimitStatus()
        self._timeline_request_limit = limits['resources']['statuses']['/statuses/user_timeline']['remaining']
        self._timeline_request_end = limits['resources']['statuses']['/statuses/user_timeline']['reset']
        self._status_request_limit = limits['resources']['statuses']['/statuses/show/:id']['remaining']
        self._status_request_end = limits['resources']['statuses']['/statuses/show/:id']['reset']
        self._timeline_requests = 0
        self._status_requests = 0

    def check_timeline_requests(self):
        """
        Make sure we've still got enough
        requests left in our timeline request
        limits
        """

        if self._timeline_requests >= self._timeline_request_limit and int(time.time()) < self._timeline_request_end:
            diff = self._timeline_request_end - int(time.time())
            LOGGER.info("API timeline request limit exceeded - sleeping for {0} seconds".format(diff))
            time.sleep(diff+1)
            self.get_limits()

    def check_status_requests(self):
        """
        Make sure we've still got enough
        requests left in our status request
        limits
        """
        if self._status_requests >= self._status_request_limit and int(time.time()) < self._status_request_end:
            diff = self._status_request_end - int(time.time())
            LOGGER.info("API status request limit exceeded - sleeping for {0} seconds".format(diff))
            time.sleep(diff+1)
            self.get_limits()

    def is_retweet(self, tweet, content_id):
        """
        @param: tweet
        @type: twitter.Status instance

        returns True if is retweet else False
        """

        tw_content_row = session.query(TWContent).filter(
                                       TWContent.content_id==content_id
                                        ).filter(
                                                TWContent.retweet==False
                                                ).order_by(TWContent.post_time).first()
        if tw_content_row and (tweet.created_at_in_seconds - tw_content_row.post_time) < 86400:
            return True
        return False


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--site-id', dest='site_id', help='site id to use', default='OMG', required=True)
    parser.add_argument('--back-populate', dest='back_populate', help='back populate this dataset')
    parser.add_argument('--start_date', dest='start_date', help='back populate start date')
    parser.add_argument('--stop_date', dest='stop_date', help='back populate stop date')
    args = parser.parse_args()
    script = Script(args.site_id, args.back_populate, args.start_date, args.stop_date).main()
